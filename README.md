# HTML TOOLS

A collection of static analysis images and tools. The main aim of this project is to create and maintain container images to support static analysis on builds in the gitlab-ci pipeline.

Images can be run locally by accessing the registry at `registry.gitlab.com/makejames/html-tools/<name-of-tool>`

## Context

There are many and varied tools to lint web files. Most offer their own subset of html style rules, but only some seem to be able to validate a directory containing a static site.

A lot of tools seem to require a running instance of the application to run, which is less than ideal. They achieve this either by accessing a direct link to the site or by spinning up a node runtime environment.

For many rules this feels unnecessary, given that analysis of the html file could easily reveal many of the anti-patterns in html files like an `<img>` tag as an `alt` value.

## HTML Validate

HTML Validate is an offline static analysis tool. Configurable using a `.htmlvalidate` file that can be saved in the root of the project.

Can accept a directory as an argument, and can be configured in a `.gitlab-ci.yml` file to accept build artefacts.

## Pa11y

Pa11y CI is an accessibility tool focussed on running in CI pipelines.
